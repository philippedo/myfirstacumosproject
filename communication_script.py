#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 31 10:42:10 2019

@author: cloud
"""
import os,sys
#sys.path.append('/home/philippe/MODELS/Python/Iris/Python3_8_iris')

import model_pb2 as pb  # R_model/IRIS
import requests

restURL = "http://model:3330/model/methods/classify"

def classify_iris (sl, sw, pl, pw):
     df=pb.IrisDataFrame()
     #df = pb.predictInput()
     df.sepal_length.append(sl)
     df.sepal_width.append(sw)
     df.petal_length.append(pl)
     df.petal_width.append(pw)

     r = requests.post(restURL, df.SerializeToString(), headers={'Content-Type': 'application/vnd.google.protobuf', 'Accept': 'application/vnd.google.protobuf'})
     of = pb.ClassifyOut()
     #of = pb.predictOutput()
     of.ParseFromString(r.content)
     #return of
     return of.value[0]
 
from sklearn.datasets import load_iris
iris = load_iris()
id = iris.data
it = iris.target
 
for i in range(len(id)):
     classify_iris(*(id[i]))
     print('Input: {}, Predicted: {}, Actual {}'.format(id[i], classify_iris(*(id[i])), it[i]))
